<?php get_header(); ?>
	<!-- section -->
	<section>
		<div class="fon">
			<h1>Добро пожаловать</h1>
		</div>

		<!-- section1 -->
		<div class="section1">
			<h1>Дополнительное образование</h1>
			<div class="container">
				<!-- block1 -->
				<div class="section_block">
					<a href="">
						<div class="section_bloki">
							<img src="<?php echo get_template_directory_uri() ?> /img/prof_1.jpg">
							<h1>СВАРЩИК</h1>
						</div>
					</a>
					<a href="">
						<div class="section_bloki">
							<img src="<?php echo get_template_directory_uri() ?> /img/prof_1.jpg">
							<h1>СВАРЩИК</h1>
						</div>
					</a>
				</div>
				<!-- block2 -->
				<div class="section_block">
					<a href="">
						<div class="section_bloki">
							<img src="<?php echo get_template_directory_uri() ?> /img/prof_1.jpg">
							<h1>СВАРЩИК</h1>
						</div>
					</a>
					<a href="">
						<div class="section_bloki">
							<img src="<?php echo get_template_directory_uri() ?> /img/prof_1.jpg">
							<h1>СВАРЩИК</h1>
						</div>
					</a>
				</div>
			</div>

			<a href=""><div class="knopka">Показать все курсы</div></a>
		</div>

		<!-- section2 -->
		<div class="section2">
			<h1>Новости</h1>
			<h4>Последние новости, мероприятия, события.</h4>
			<div class="container">
				<!-- block1 -->
				<div class="section_block">
					<div class="section_bloki">
						<img src="<?php echo get_template_directory_uri() ?> /img/header_fon.jpg">
						<p>новость новость новость новость новость новость</p>
						<a href=""><div class="knopka1">Подробнее</div></a>	
					</div>
					<div class="section_bloki">
						<img src="<?php echo get_template_directory_uri() ?> /img/header_fon.jpg">
						<p>новость новость новость новость новость новость</p>
						<a href=""><div class="knopka1">Подробнее</div></a>	
					</div>
				</div>
				<!-- block2 -->
				<div class="section_block">
					<div class="section_bloki">
						<img src="<?php echo get_template_directory_uri() ?> /img/header_fon.jpg">
						<p>новость новость новость новость новость новость</p>
						<a href=""><div class="knopka1">Подробнее</div></a>	
					</div>
					<div class="section_bloki">
						<img src="<?php echo get_template_directory_uri() ?> /img/header_fon.jpg">
						<p>новость новость новость новость новость новость</p>
						<a href=""><div class="knopka1">Подробнее</div></a>	
					</div>
				</div>
			</div>
			<a href=""><div class="knopka">Показать все новости</div></a>
		</div>

		<!-- section3 -->
		<div class="section3">
			<div class="container">
				<!-- block1 -->
				<div class="section3_block">
					<h1>Связатсья с нами</h1>
					<p>Если у вас возникли вопросы, мы с радостью на них 
					ответим, пожалуйста, заполните форму и мы с вам 
					перезвоним </p>
					<form>
						<input type="text" name="" placeholder="ФИО"><br>
						<input type="text" name="" placeholder="Номер"><br>
						<input type="text" name="" placeholder="Email"><br>
						<span>Вопрос/Комментарий</span> <br>
						<input type="comment" name="" placeholder=""><br>
						<button>Отправить</button>
					</form>
				</div>
				<!-- block2 -->
				<div class="section3_block">
					<img src="<?php echo get_template_directory_uri() ?> /img/img1.jpg">
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>