<!DOCTYPE html>
<html lang="ru">
<head>
	<?php wp_head(); ?>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<!-- header -->
	<header>
		<div class="container">
			<a href="" class="logo">
				<img src="img/logotip.jpg">
				<span class="text_logo">ПРОФЕССИОНАЛЬНЫЙ ЛИЦЕЙ</span>
			</a>

			<div>
				<?php wp_nav_menu('top') ?>
			</div>
			<div class="menu_m">
				<div class="toggle" onclick="document.querySelector('.toggle1').style.display = 'block'">
					MENU
				</div>
				<div class="toggle1">
					<h1>MENU</h1>
					<?php wp_nav_menu('top') ?>
					<br>
					<div class="toggle" onclick="document.querySelector('.toggle1').style.display = 'none'">
						BACK
					</div>
				</div>
			</div>
		</div>
	</header>